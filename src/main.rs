// external dependencies
extern crate ssh2;
extern crate colored;
#[macro_use]
extern crate clap;

extern crate pginx;

// eternal imports
use colored::*;

use pginx::*;

fn main() {

    let yml = load_yaml!("args.yml");
    let m = clap::App::from_yaml(yml).get_matches();

    // 
    let mut domains: Vec<String> = Vec::new();


    // Connect to remote server 
    let connect_uri = m.value_of("connect_uri").unwrap();
    let connection =  match SslConnection::new(&connect_uri) {
        Ok(con) => con,
        Err(_) => {
            println!("Failed to open ssh connection to {}", connect_uri);
            return;
        }
    };
    println!("{}", "Connected".bright_green());

    if m.is_present("list") || m.is_present("save") {
        domains = nginx_config::connect_and_list_domains(&connection);
    }

    // list hosted domains
    if m.is_present("list") {
        println!("domains served by {} {:#?}", connection.get_uri(), domains);
    }

    // save hosted domains 
    if m.is_present("save") {
        match host_file::backup() {
            Err(e) => match e {
                Error::GenericError(err) => println!("{}", err),
                Error::IoError(e) => {
                    println!("{}", e);
                    return;
                },
                _ => ()
            }
            Ok(path) => println!("backed up file to {:?}", path)
        }

        // Backup was successful
        host_file::append_new_domains(&domains, &connection.get_ip()).unwrap();
    }

    if m.is_present("restore_hostfile") {
        host_file::restore_backup().unwrap();
    }

    if m.is_present("backup") {
        let destination = m.value_of("backup").unwrap();
        nginx_config::backup_configs(&connection, destination).unwrap();
    }

    if m.is_present("add_config") {
        let source = m.value_of("add_config").unwrap();
        nginx_config::send_config(&connection, source)
    }

}
