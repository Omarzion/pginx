extern crate ssh2;
extern crate rpassword;
#[macro_use]
extern crate cfg_if;
extern crate colored;
extern crate zip;


use std::io;
use std::net::TcpStream;
use std::path::{ PathBuf, Path };
use std::fs;
use std::ffi::OsString;

use rpassword::prompt_password_stdout;
use ssh2::{ Session, Channel, Sftp };
use colored::*;

#[allow(dead_code)]
pub struct SslConnection {
  pub username: String,
  session_uri: String,
  session: Session,
  tcp_stream: TcpStream
}

#[derive(Debug)]
pub enum Error {
  IoError(io::Error),
  SshError(ssh2::Error),
  GenericError(String)
}

impl From<io::Error> for Error {
  fn from(error: io::Error) -> Self {
    Error::IoError(error)
  }
}

impl From<ssh2::Error> for Error {
  fn from(error: ssh2::Error) -> Self {
    Error::SshError(error)
  }
}

pub trait ContainedChannel {
  fn close(&mut self) -> Result<i32, Error>;
}

impl<'t> ContainedChannel for Channel<'t> {
  fn close(&mut self) -> Result<i32, Error> {
    self.close()?;
    self.wait_close()?;
    let status = self.exit_status()?;
    Ok(status)
  }
}

impl SslConnection {

  // connect_uri expected format user@ip[:port]
  pub fn new(connect_uri: &str) -> Result<SslConnection, Error> {
    let mut connect_uri = String::from(connect_uri);

    // if connect_uri doesn't include a port number
    // append the default :22
    if connect_uri.split(':').collect::<Vec<&str>>().len() < 2 {
      connect_uri += ":22";
    }

    // split the connect_uri into username and ip:port
    let split: Vec<&str> = connect_uri.split('@').collect();
    let username = String::from(split[0]);
    let session_uri = String::from(split[1]);


    let tcp_stream = TcpStream::connect(&session_uri)?;
    let mut session = Session::new().ok_or(Error::GenericError("Failed to create session".to_string()))?;

    session.handshake(&tcp_stream)?;

    let conn = SslConnection {
      username,
      session_uri,
      session,
      tcp_stream
    };
    if !conn.authenticate()? {
      return Err(Error::GenericError(format!("Failed to login to {}", connect_uri)));
    }

    Ok(conn)
  }

  pub fn get_ip(&self) -> String {
    String::from(self.session_uri.split(":").collect::<Vec<_>>()[0])
  }



  pub fn get_uri(&self) -> String {
    self.session_uri.clone()
  }

  fn authenticate(&self) -> Result<bool, Error> {
    // Try to authenticate with ssh-agent
    if self.session.userauth_agent(&self.username).is_err() {
      println!("Failed to login with ssh agent");

      let auth_success = |pass: &str| self.session.userauth_password(&self.username, pass).is_ok() || self.session.authenticated();

      // Try to login with username and password - give them 5 attempts
      let pass: String = prompt_password_stdout(&format!("Password for {}: ", &self.username))?;
      if !auth_success(&pass) {
        for attempt in 2..=5 {
          println!("Login attempt failed");
          let pass: String = prompt_password_stdout(&format!("Password for {} attempt {}: ", &self.username, attempt))?;
          if auth_success(&pass) {
            break;
          }
        }
        if self.session.authenticated() {
          return Err(Error::GenericError(format!("Failed to authenticate {} on {}", &self.username, &self.session_uri)));
        }
      }
    }
    Ok(true)
  }

  pub fn get_channel(&self) -> Result<Channel, Error> {
    let channel = self.session.channel_session()?;
    Ok(channel)
  }

  pub fn get_sftp(&self) -> Result<Sftp, Error> {
    let sftp = self.session.sftp()?;
    Ok(sftp)
  }
}

pub mod nginx_config {
  use std::io::{ Read, Write };
  use ::*;

  pub fn parse_servernames(s: &str) -> Vec<&str> {
    let mut lines: Vec<&str> = s.split('\n')
      .into_iter()
      .map(|line| line.trim())
      .filter(|line| line.starts_with("server_name"))
      .flat_map(strip_servername_line)
      .collect::<Vec<_>>();

    lines.sort();
    lines.dedup();
    lines
  }

  fn strip_servername_line(s: &str) -> Vec<&str> {
    let mut words: Vec<&str> = s.split_whitespace().rev().into_iter().collect();
    words.pop();
    words.into_iter().map(|uri| uri.trim_right_matches(';')).collect()
  }

  pub fn backup_configs(conn: &SslConnection, destination: &str) -> Result<(), Error> {
    let sftp = conn.get_sftp()?;

    let files = sftp.readdir(Path::new("/etc/nginx/sites-enabled"))?;
    let mut unreadable_files: Vec<(PathBuf, Error)> = Vec::new();
    let file = fs::File::create(&destination)?;
    let mut zip = zip::ZipWriter::new(file);
    let options = zip::write::FileOptions::default()
      .compression_method(zip::CompressionMethod::Bzip2);

    let mut buffer = Vec::new();
    for (filename, _) in files {
      let ok = match sftp.open(&filename) {
        Ok(mut file) => match file.read_to_end(&mut buffer) {
          Err(e) => { unreadable_files.push((filename.clone(), Error::IoError(e))); false },
          Ok(_) => true
        }
        Err(e) => { unreadable_files.push((filename.clone(), Error::SshError(e))); false }
      };

      if ok {
        let filename = OsString::from(filename.file_name().unwrap()).into_string().unwrap();
        zip.start_file(filename.clone(), options).unwrap();
        zip.write_all(&*buffer).unwrap();
        println!("adding {} to {}", &filename, &destination);
      }
      buffer.clear();

      // println!("{:?}", filename.file_name());

    }
    zip.finish().unwrap();
    Ok(())
  }

  pub fn send_config(conn: &SslConnection, filename: &str) {
    let sftp = conn.get_sftp().unwrap();
    let mut channel = conn.get_channel().unwrap();
    create_config(&sftp, &mut channel, filename)
  }

  fn create_config(sftp: &Sftp, channel: &mut Channel, filename: &str) {
    let source_file = Path::new(filename);
    let path = Path::new("/etc/nginx/sites-available").join(source_file.file_name().unwrap());
    let mut file_tx = sftp.create(&path).unwrap();
    let mut file_rx = fs::File::open(source_file).unwrap();
    let mut buffer = Vec::new();
    let mut nginx_test = String::new();
    
    file_rx.read_to_end(&mut buffer).unwrap();
    file_tx.write_all(&buffer).unwrap();
    file_tx.flush().unwrap();
    channel.exec("nginx -t").unwrap();

    channel.read_to_string(&mut nginx_test).unwrap();
    println!("{:?}", nginx_test);
  }

  pub fn connect_and_list_domains(conn: &SslConnection) -> Vec<String> {
    let sftp = conn.get_sftp().unwrap();

    let mut files = sftp.readdir(Path::new("/etc/nginx/sites-enabled")).unwrap();

    let mut uris: Vec<String> = Vec::new();
    let mut unreadable_files: Vec<(PathBuf, Error)> = Vec::new();

    let mut buffer = String::new();
    while files.len() > 0 {
        let (filename, _stats) = files.pop().unwrap();
        
        match sftp.open(&filename) {
            Ok(mut file) => match file.read_to_string(&mut buffer) {
                Err(e) => unreadable_files.push((filename, Error::IoError(e))),
                Ok(_) => ()
            },
            Err(e) => unreadable_files.push((filename, Error::SshError(e)))
        };
        uris.append(&mut parse_servernames(&buffer).into_iter().map(|x| String::from(x)).collect());
        buffer.clear();
    }
    if unreadable_files.len() > 0 {
        println!("failed to open some files:\n{:#?}", &unreadable_files);
    }
    uris
  }
}

pub mod host_file {
  use std::path::{ Path, PathBuf };
  use std::io::{ self, prelude::*};
  use std::fs::{ self,};
  use ::*;

  cfg_if! {
    if #[cfg(unix)] {
      fn get_path() -> String {
        String::from("/etc")
      } 
    } else if #[cfg(windows)] {
      fn get_path() -> String {
        String::from("C:\\Windows\\System32\\drivers\\etc")
      }
    }
  }

  pub fn backup() -> Result<PathBuf, Error> {
    let path = get_path();
    let filename = Path::new(&path);
    let abosolute_destination = fs::canonicalize(filename.join("hosts.pginx.old"))?;
    if !filename.join("hosts.pginx.old").exists() {
      fs::copy(filename.join("hosts"), filename.join("hosts.pginx.old"))?;
    } else {
      return Err(Error::GenericError(format!("Backup file {:?} already exists", abosolute_destination)));
    }
    Ok(abosolute_destination)
  }

  fn parse() -> io::Result<Vec<String>> {
    let path = get_path();
    let file = fs::OpenOptions::new().read(true).open(Path::new(&path).join("hosts"))?;
    let mut contents = String::new();
    io::BufReader::new(file).read_to_string(&mut contents)?;
    Ok(contents.split("\n").filter(|line| {
      let chars = line.chars().collect::<Vec<char>>();
      if chars.len() < 1 || chars[0] == '#' { return false };
      true
    }).map(|s| String::from(s)).collect())
  }

  pub fn append_new_domains(domains: &Vec<String>, ip: &str) -> io::Result<()> {
    let path = get_path();
    let existing_domains = parse()?;
    let unique_domains: Vec<&String> = domains.into_iter().filter(|domain| !existing_domains.clone().into_iter().any(|d| d.contains(&domain[..]))).collect::<Vec<&String>>();
    // let duplicate_domains: Vec<&String> = domains.into_iter().filter(|domain| existing_domains.clone().into_iter().any(|d| d.contains(&domain[..]))).collect::<Vec<&String>>();
    // println!("duplicate domains {:#?}", &duplicate_domains);

    let mut file = fs::OpenOptions::new().append(true).open(Path::new(&path).join("hosts"))?;
    for domain in &unique_domains {
      writeln!(file, "{}\t{}", ip, domain)?;
      println!("Adding {} to host file", domain.green());
    }

    if unique_domains.len() < 1 {
      println!("Nothing to be added to hosts file");
    }

    Ok(())
  }

  pub fn restore_backup() -> Result<(), Error> {
    let path = get_path();
    let filename = Path::new(&path);
    if filename.join("hosts.pginx.old").exists() {
      fs::rename(filename.join("hosts.pginx.old"), filename.join("hosts"))?;
    }
    Ok(())
  }
}
